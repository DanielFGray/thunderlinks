module.exports = {
  up: knex => knex.schema.createTable('links', t => {
    t.increments('id').primary()
    t.string('title')
    t.string('url')
    t.string('tags')
  }),
  down: knex => knex.schema.dropTable('links'),
}
