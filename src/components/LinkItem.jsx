import React from 'react'

export default class LinkItem extends React.Component {
  state = {
    editing: false,
    tagInput: this.props.tags,
  }

  tagChange = e => {
    this.setState({ tagInput: e.currentTarget.value })
  }

  updateTags = e => {
    e.preventDefault()
    this.props.updateTags(this.state.tagInput)
    this.setState({ editing: false })
  }

  startEdit = () => {
    this.setState({ editing: true })
  }

  render() {
    const {
      url, title, tags, tagClick,
    } = this.props
    const { tagInput, editing } = this.state
    return (
      <div className="linkItem">
        <a
          href={url}
          target="_blank"
          rel="noopener noreferrer"
        >
          {title}
        </a>
        {editing ? (
          <form onSubmit={this.updateTags}>
            <input
              value={tagInput}
              onChange={this.tagChange}
            />
          </form>
        ) : (
          tags
            .split(/,\s*/)
            .filter(Boolean)
            .map(tag => (
              <span className="tag" onClick={tagClick(tag)}>
                {tag}
              </span>
            ))
            .concat(
              <span className="tag" onClick={this.startEdit}>
                +
              </span>,
            )
        )}
      </div>
    )
  }
}
