import React from 'react'
import {
  curry,
  map,
  filter,
  isEmpty,
  lensPath,
  over,
  pipe,
  pluck,
  propSatisfies,
  split,
  toLower,
  unless,
} from 'ramda'
import LinkItem from './components/LinkItem'

const on = curry((f, g, a, b) => f(g(a))(g(b)))
const includes = curry((a, b) => b.includes(a))
const includesI = on(includes, toLower)
const flatten = x => x.reduce((p, c) => p.concat(c), [])
const uniq = x => Array.from(new Set(x))

const getTags = pipe(
  pluck('tags'),
  map(split(/,\s*/)),
  flatten,
  uniq,
)

const filterData = ({ selectedTag, search, data }) => pipe(
  unless(
    () => isEmpty(selectedTag),
    filter(propSatisfies(includes(selectedTag), 'tags')),
  ),
  filter(propSatisfies(includesI(search), 'title')),
)(data)

export default class Main extends React.Component {
  state = {
    search: '',
    selectedTag: '',
    data: [],
  }

  componentDidMount() {
    fetch('/api/v1/links')
      .catch(console.error)
      .then(x => x.json())
      .then(data => this.setState({ data }))
  }

  tagClick = selectedTag => () => {
    this.setState({ selectedTag })
  }

  filterChange = e => {
    this.setState({ selectedTag: e.currentTarget.value })
  }

  searchChange = e => {
    this.setState({ search: e.currentTarget.value })
  }

  updateTags = curry((id, tags) => {
    this.setState(
      over(lensPath(['data', id, 'tags']), () => tags),
    )
  })

  render() {
    const { selectedTag, search, data } = this.state
    const filteredData = filterData(this.state)
    return (
      <div>
        <div>
          <input
            onChange={this.searchChange}
            value={search}
          />
          <select
            value={selectedTag}
            onChange={this.filterChange}
          >
            {['']
              .concat(getTags(data))
              .map(tag => (
                <option value={tag}>
                  {tag}
                </option>
              ))}
          </select>
        </div>
        <div>
          {filteredData.map(x => (
            <LinkItem
              {...x}
              updateTags={this.updateTags(x.id)}
              tagClick={this.tagClick}
            />
          ))}
        </div>
      </div>
    )
  }
}
