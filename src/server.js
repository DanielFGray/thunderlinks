/* eslint-disable no-console */
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const bodyParser = require('body-parser')
const Joi = require('joi')
const db = require('./db')

const app = express()

app.use(morgan('dev'))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const staticPath = path.join(__dirname, '..', 'public')
app.use(express.static(staticPath))

app.get('/', (req, res) => {
  res.sendFile('index.html', {
    root: path.join(__dirname, '..', 'public'),
  })
})

app.get('/api/v1/links', (req, res) => {
  db('links')
    .select()
    .then(data => res.json(data))
})

app.post('/api/v1/links', (req, res) => {
  const schema = {
    url: Joi.string()
      .uri()
      .required(),
    title: Joi.string().required(),
  }
  Promise.resolve(Joi.validate(req.body, schema))
    .then(body => db('links').insert(body))
    .then(() => db('links').select())
    .then(body => res.json({ status: 'ok', body }))
    .catch(x => {
      const err = {
        status: 'error',
        body: x.details.map(({ message }) => message),
      }
      res.status(400).json(err)
    })
})

const port = process.env.PORT || 8765
const host = process.env.HOST || 'localhost'

app.listen(port, host, () => console.log(
  `server now running on http://${host}:${port}`,
))
