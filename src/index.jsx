import React from 'react'
import ReactDOM from 'react-dom'
import Main from './Main'
import './style.sss'

ReactDOM.render(<Main />, document.getElementById('main'))
