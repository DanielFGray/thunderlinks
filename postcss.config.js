module.exports = {
  parser: 'sugarss',
  env: {
    production: {
      cssnano: { autoprefixer: false },
    },
  },
}
